package pl.szymson.projekt.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.szymson.projekt.model.File;

@Repository
public interface FileRepository extends JpaRepository<File, Long> {
	List<File> findByUploadBy(String email);
}