package pl.szymson.projekt.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.szymson.projekt.model.FileDestination;

@Repository
public interface FileDestinationRepository extends JpaRepository<FileDestination, Long>{
	List<FileDestination> findByDstUserEmail(String email);
}
