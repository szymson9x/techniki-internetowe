package pl.szymson.projekt.exceptions;

public class ProjektException extends RuntimeException { 
    public ProjektException(String errorMessage) {
        super(errorMessage);
    }
}