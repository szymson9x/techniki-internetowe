package pl.szymson.projekt.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import pl.szymson.projekt.dto.FileDto;

public interface StoreService {
	 void store(MultipartFile file, String name);
}
