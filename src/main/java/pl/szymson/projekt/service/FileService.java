package pl.szymson.projekt.service;

import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import pl.szymson.projekt.dto.FileDto;
import pl.szymson.projekt.model.File;

public interface FileService {

    void insertFile(MultipartFile file, FileDto dto);
    
    List<File> getFilesFrom(String email);
    
    List<File> getFilesTo(String email);

    Stream<Path> loadAll();

    Path load(String filename);

    void deleteAll();

	File getFileById(Long id);

	ResponseEntity<Resource> loadAsResource(Long id);
}