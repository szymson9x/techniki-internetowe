package pl.szymson.projekt.service.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import pl.szymson.projekt.exceptions.ProjektException;
import pl.szymson.projekt.service.StoreService;

@Service
public class StoreServiceImpl implements StoreService{
	
	 @Value("${projekt.store.url}")
	 private String url;

	@Override
	public void store(MultipartFile file, String name) {
		
		byte[] bytes;
		try {
			bytes = file.getBytes();
			Path path = Paths.get(url + name);
		    Files.write(path, bytes);
		} catch (IOException e) {
			throw new ProjektException(e.getMessage());
		}
	}

}
