package pl.szymson.projekt.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.okta.sdk.authc.credentials.TokenClientCredentials;
import com.okta.sdk.client.Client;
import com.okta.sdk.client.Clients;

import pl.szymson.projekt.service.OktaService;

@Service
public class OktaServiceImpl implements OktaService{
	
	 @Value("${okta.oauth2.orgUrl}")
	 private String url;
	 
	 @Value("${okta.oauth2.apiSecret}")
	 private String secret;

	@Override
	public List<String> getUsers() {
		Client client = Clients.builder()
			    .setOrgUrl(url)
			    .setClientCredentials(new TokenClientCredentials(secret))
			    .build();
		
		List<String> users = client.listUsers().stream().map(user -> user.getProfile().getEmail()).collect(Collectors.toList());
		return users;
	}

}
