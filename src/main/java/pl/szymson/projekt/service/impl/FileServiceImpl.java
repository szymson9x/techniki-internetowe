package pl.szymson.projekt.service.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import pl.szymson.projekt.dto.FileDto;
import pl.szymson.projekt.model.File;
import pl.szymson.projekt.model.FileDestination;
import pl.szymson.projekt.repo.FileDestinationRepository;
import pl.szymson.projekt.repo.FileRepository;
import pl.szymson.projekt.service.FileService;
import pl.szymson.projekt.service.StoreService;
import pl.szymson.projekt.tools.FileNameGenerator;

@Service
public class FileServiceImpl implements FileService{
	
	@Autowired
	private FileRepository fileRepository;
	
	@Autowired
	private FileDestinationRepository fileDestinationRepository;
	
	@Autowired
	private StoreService storeService;
	
	@Value("${projekt.store.url}")
	private String url;

	@Override
	@Transactional
	public void insertFile(MultipartFile file, FileDto dto) {
		String tempName = FileNameGenerator.getSaltString();
		

		FileDestination fd = FileDestination.builder()
				.dstUserEmail(dto.getDestUser())
				.createdAt(new Date())
				.updatedAt(new Date())
				.build();
		
		File f = File.builder()
				.orygName(file.getOriginalFilename())
				.tempName(tempName)
				.destinationFiles(new ArrayList<FileDestination>(Arrays.asList(fd)))
				.uploadBy(dto.getSourceUser())
				.createdAt(new Date())
				.updatedAt(new Date())
				.build();
		fd.setFile(f);
		
		fileRepository.save(f);
		storeService.store(file, tempName);
	}

	@Override
	public Stream<Path> loadAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Path load(String filename) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Resource> loadAsResource(Long id) {
		File f = fileRepository.getOne(id);
		java.io.File file = new java.io.File(url + f.getTempName());
		
		 Path path = Paths.get(file.getAbsolutePath());
		 ByteArrayResource resource = null;
		try {
			resource = new ByteArrayResource(Files.readAllBytes(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		 
		 HttpHeaders header = new HttpHeaders();
	        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename="+f.getOrygName());
	        header.add("Cache-Control", "no-cache, no-store, must-revalidate");
	        header.add("Pragma", "no-cache");
	        header.add("Expires", "0");
		 

	    return ResponseEntity.ok()
	            .headers(header)
	            .contentLength(file.length())
	            .contentType(MediaType.parseMediaType("application/octet-stream"))
	            .body(resource);
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<File> getFilesFrom(String email) {
		return fileRepository.findByUploadBy(email);
	}

	@Override
	public List<File> getFilesTo(String email) {
		return fileDestinationRepository.findByDstUserEmail(email).stream().map(dst -> dst.getFile()).collect(Collectors.toList());
	}
	
	@Override
	public File getFileById(Long id) {
		File f = fileRepository.getOne(id);
		
		return f;
	}

}
