package pl.szymson.projekt.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor
public class FileDto {
	private String sourceUser;
	private String destUser;
}
