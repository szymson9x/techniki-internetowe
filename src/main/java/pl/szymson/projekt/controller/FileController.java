package pl.szymson.projekt.controller;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.domain.JpaSort.Path;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import pl.szymson.projekt.dto.FileDto;
import pl.szymson.projekt.model.File;
import pl.szymson.projekt.service.FileService;
import pl.szymson.projekt.service.OktaService;

@Controller
public class FileController {
	
	@Autowired
	private FileService fileService;
	
	@Autowired
	private OktaService oktaService;
	
	@RequestMapping(value = "/uploadForm", method = RequestMethod.GET)
    public String index(Principal principal,Model model,
	        HttpServletRequest request) {
		
		List<String> users = oktaService.getUsers();
		model.addAttribute("users", users);

        return "uploadForm";
    }

	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    public String handleFileUpload(@RequestParam("file") MultipartFile file,
    		@RequestParam("user") String destination,
            RedirectAttributes redirectAttributes, Principal principal
	    ) {
		
		fileService.insertFile(file, new FileDto(principal.getName(), destination));
		
        redirectAttributes.addFlashAttribute("message",
                "You successfully uploaded " + file.getOriginalFilename() + "!");

        return "redirect:/";
    }
	
	@RequestMapping(value = "/listFiles", method = RequestMethod.GET)
    public String listFiles(Principal principal,Model model) {
		
		List<File> filesFrom = fileService.getFilesFrom(principal.getName());
		model.addAttribute("filesFrom",filesFrom);
		
		List<File> filesTo = fileService.getFilesTo(principal.getName());
		model.addAttribute("filesTo",filesTo);

        return "files";
    }
	
	@RequestMapping(value = "/file/{id}", method = RequestMethod.GET)
    public ResponseEntity<Resource> downloadFile(@PathVariable String id, Principal principal,Model model) {
		
		return fileService.loadAsResource(Long.parseLong(id));
    }
}
