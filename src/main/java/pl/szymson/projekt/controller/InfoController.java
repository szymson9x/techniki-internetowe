package pl.szymson.projekt.controller;

import java.util.Collections;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class InfoController {

	 @GetMapping("/profile")
     @PreAuthorize("hasAuthority('Admin')")
     public ModelAndView userDetails(OAuth2AuthenticationToken authentication) {
         return new ModelAndView("userProfile" , Collections.singletonMap("details", authentication.getPrincipal().getAttributes()));
     }
}
