package pl.szymson.projekt.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.szymson.projekt.service.OktaService;

@Controller
public class UserController {
	
	@Autowired
	private OktaService oktaService;
	
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String users(Model model,
	        HttpServletRequest request,
	        @RequestParam(name = "state", required = false) String springState
	    ) {
		
		List<String> users = oktaService.getUsers();
		model.addAttribute("users", users);
		
		return springState;
	}
	
}
